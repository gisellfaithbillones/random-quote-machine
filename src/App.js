import logo from './logo.svg';
import React from 'react';
import './App.css';
import QuoteAndAuthor from './component/QuoteAndAuthor';

function App() {
  return (
    <>
        <QuoteAndAuthor/>
    </>
  );
}

export default App;
