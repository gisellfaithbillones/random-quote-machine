import React from 'react';
import ReactFCCtest from 'react-fcctest';
import { SocialIcon } from 'react-social-icons';



export default function QuoteAndAuthor() {

    const endpoint = 'https://api.quotable.io/random'

    async function getQuote() {
        try {
            const response = await fetch(endpoint);
            if (!response.ok) {
                throw Error(response.statusText)
            }

            const quote = await response.json();
            console.log({quote});
            console.log({content: quote.content});
            console.log({author: quote.author});
            displayQuote(quote.content);
            displayAuthor(quote.author);
            shareQuote();
        } catch (err) {
            console.log(err);
            alert('Failed to fetch new quote');
        }

    }

    function displayQuote(quote) {
        const quoteText = document.querySelector('#js-quote-text');
        quoteText.innerText = quote;
    }

    function displayAuthor(author) {
        const authorName = document.querySelector('#author');
        authorName.innerText = author;
    }

    function shareQuote(quote, author) {
        const twitter = document.querySelector('#tweet-quote');
        twitter.setAttribute(
            'href',
            'https://twitter.com/intent/tweet?hashtags=quotes,Fcc&related=freecodecamp&text=' +
            encodeURIComponent('"' + quote + '" ' + author)
        )
    }

    return(
        <>
            <ReactFCCtest/>
            <div className="app">
                <header>Random Quotes</header>
                <div id='quote-box'>
                    <section className="quotes" id='text'>
                        <div id="js-quote-text"></div>
                        <br/>
                        <div id='author'></div>
                    </section>
                    <section className="controls">
                        <a id='new-quote' className='button randomBgColor' onClick={getQuote}>Generate a new quote</a>
                        <a
                            className='button'
                            id="tweet-quote"
                            target="blank"
                            href="twitter.com/intent/tweet"
                            data-text="custom share text"
                        >
                            <SocialIcon url="https://twitter.com/jaketrent" />                        </a>
                    </section>
            </div>
            </div>
            </>
    )
}